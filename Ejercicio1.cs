using System;

namespace ejercicio_datos_basicos_master
{
    class Ejercicio1
    {
        static void Main(string[] args)
        {
           /* #1 Preguntar al usuario su edad, que se guardará en un "byte".
            A continuación, se deberá le deberá decir que no aparenta tantos años (por ejemplo, "No aparentas 20 años").*/

            byte Edad;
            Console.WriteLine("¿Cual es su edad?");
            Edad = Convert.ToByte(Console.ReadLine()); 

 
            Console.WriteLine("No aparentas {0} años de edad ", Edad );


        }
    }
}
