using System;

namespace ejercicio_datos_basicos_master
{
    class Ejercicio3
    {
        static void Main(string[] args)
        {
           /* #3 Crear un programa que use tres variables x,y,z. Sus valores iniciales serán 15, -10, 2.147.483.647.
            Se deberá incrementar tres vece el valor de estas variables. Mostrar en pantalla el valor incial de las variables
             y el valor final obtenido por el programa.*/

             double  X,Y,Z;
               X  = 15;
               Y = -10;
               Z = 2.147483647;
               Console.WriteLine("El valor inicial x es {0}, el resultado final es {1} ", X, X *3);

               Console.WriteLine("El valor inicial de y es {0}, el valor final es {1} ", Y, Y *3);

               Console.WriteLine("El valor inicial de z es {0}, el valor final es {1}", Z, Z*3);


        }
    }
}
